package exam;

public class MaxMinProduct {
	public static void main(String[] args) {
		int[] intArray = { 25,38,11,1,-5,37,41,23,6,8,-11 };
	    int min = intArray[0];
	    int max = intArray[0];
	    int result;
	    
	    for(int i=1; i<intArray.length; i++){ 
	      if(intArray[i] > max){ 
	    	  max = intArray[i]; 
	      }
	      else if(intArray[i] < min) {
	          min = intArray[i];
	      }
	    }
	    result = min*max;
	    System.out.println("Result: " + result);
		
	}
}

