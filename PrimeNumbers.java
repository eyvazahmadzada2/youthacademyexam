package exam;

import java.util.Scanner;

public class PrimeNumbers {
	
	static boolean isPrime(int n) { 
		if (n <= 1) 
		    return false;
		  
		for (int i = 2; i < n; i++) 
		    if (n % i == 0) 
		        return false; 
		  
		return true; 
	} 
	   
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a non-negative integer: ");
		int n = scanner.nextInt();
		int counter = 0;
	    
		for (int i = 2; i <= n; i++) { 
		    if (isPrime(i)) {
		    	counter++;
		    }
		}
		System.out.println("There are "+counter+" prime numbers less than "+n);
	} 
} 
